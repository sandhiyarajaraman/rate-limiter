ENV["RAILS_ENV"] ||= 'test'

require File.expand_path('../config/environment', __dir__)
require 'rspec/rails'
require 'shoulda-matchers'

RSpec.configure do |config|
  config.include Shoulda::Matchers::ActiveModel, type: :model

  config.expect_with :rspec do |c|
    c.syntax = :expect
  end

  config.before(:each) do
    Rails.cache.clear
    Mongoid.purge!
  end
end
