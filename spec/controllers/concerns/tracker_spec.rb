require 'rails_helper'

RSpec.describe Tracker, type: :controller do
  controller(ApplicationController) do
    include Tracker

    def random
      render :json => {message: "success"}, status: :ok
    end
  end
  
  let(:request_path) { "anonymous/random" }

  before do
    routes.draw {
      get 'random' => 'anonymous#random'
    }
  end

  describe 'Tracking IP alone - without request url path' do
    let(:configuration) do
      JSON.parse({
        request_path: "*",
        time_window: 1,
        max_allowed: 1,
        track_pathwise: false
      }.to_json, object_class: OpenStruct)
    end

    before do
      Timecop.freeze(Time.zone.now)
    end
  
    after do
      Timecop.return
    end

    context 'when included in controller, WIHTOUT endpoint_configuration' do
      it 'succeeds when valid, fails when blocked' do
        allow(EndpointConfiguration).to receive(:default_configuration).and_return(configuration)
        get :random
        # when sum-within-window is WITHIN LIMITS
        expect(response.status).to be(200)
        expect(Rails.cache.read(request.ip)).to be_instance_of(Hash)
        expect(Rails.cache.read(request.ip)[0]).to eq(Tracker::IP_STATUS_ACTIVE)
  
        # when sum-within-window is OUTSIDE LIMITS (max 1 request per minute)
        get :random
        expect(response.status).to be(429)
        expect(Rails.cache.read(request.ip)[0]).to eq(Tracker::IP_STATUS_BLOCKED)
        expect(Rails.cache.read("#{request_path}-#{request.ip}")).to be_nil
      end
    end
  end

  describe 'Tracking IP + Request URL PATH' do
    let(:configuration) do
      JSON.parse({
        request_path: request_path,
        time_window: 5,
        max_allowed: 1,
        track_pathwise: true
      }.to_json, object_class: OpenStruct)
    end

    before do
      Timecop.freeze(Time.zone.now)
    end
  
    after do
      Timecop.return
    end

    context 'when included in controller, WITH endpoint_configuration' do
      let(:blocked_upto_timestamp) { (Time.zone.now + 5.minutes).change(:sec => 0).to_i }
      it 'succeeds when valid, fails when blocked' do
        allow(EndpointConfiguration).to receive(:find_by).and_return(configuration)
        get :random
        # when sum-within-window is WITHIN LIMITS
        expect(response.status).to be(200)
        expect(Rails.cache.read("#{request_path}-#{request.ip}")).to be_instance_of(Hash)
        expect(Rails.cache.read("#{request_path}-#{request.ip}")[0]).to eq(Tracker::IP_STATUS_ACTIVE)

        # when sum-within-window is OUTSIDE LIMITS (max 1 request per minute)
        get :random
        expect(response.status).to be(429)
        expect(Rails.cache.read("#{request_path}-#{request.ip}")[0]).to eq(Tracker::IP_STATUS_BLOCKED)
        expect(Rails.cache.read("#{request_path}-#{request.ip}")[blocked_upto_timestamp]).to eq(0)
        expect(Rails.cache.read(request.ip)).to be_nil
      end
    end
  end

  describe 'Purging old cache entries + Blocking for next x minutes' do
    let(:configuration) do
      JSON.parse({
        request_path: request_path,
        time_window: 2,
        max_allowed: 4,
        track_pathwise: true
      }.to_json, object_class: OpenStruct)
    end

    before do
      Timecop.freeze(Time.zone.now)
    end
  
    after do
      Timecop.return
    end

    let(:three_minutes_ago) { (Time.zone.now - 3.minutes).change(:sec => 0).to_i }
    let(:two_minutes_ago) { (Time.zone.now - 2.minutes).change(:sec => 0).to_i }
    let(:one_minute_ago) { (Time.zone.now - 1.minute).change(:sec => 0).to_i }
    let(:new_entry_timestamp) { Time.zone.now.change(:sec => 0).to_i }
    let(:blocked_till_begining_of_timestamp) { (Time.zone.now + 1.minutes).change(:sec => 0).to_i }

    let(:expected_wait_seconds) { blocked_till_begining_of_timestamp - Time.zone.now.to_i }

    let(:existing_cache_data) do
      {
        0 => Tracker::IP_STATUS_ACTIVE,
        three_minutes_ago => 1,
        two_minutes_ago => 2,
        one_minute_ago => 3
      }
    end

    context 'when entries older than latest-window exist' do
      it 'deletes them when calculating latest average + makes entry for next available request slot' do
        allow(EndpointConfiguration).to receive(:find_by).and_return(configuration)
        allow(Rails.cache).to receive(:read).and_return(existing_cache_data)

        expect(Rails.cache.read("#{request_path}-#{request.ip}")[three_minutes_ago]).to eq(1)
        expect(Rails.cache.read("#{request_path}-#{request.ip}")[two_minutes_ago]).to eq(2)
        expect(Rails.cache.read("#{request_path}-#{request.ip}")[one_minute_ago]).to eq(3)

        get :random

        expect(response.status).to be(200)
        expect(Rails.cache.read("#{request_path}-#{request.ip}")).to be_instance_of(Hash)
        expect(Rails.cache.read("#{request_path}-#{request.ip}")[0]).to eq(Tracker::IP_STATUS_ACTIVE)

        # delete obsolete entries
        expect(Rails.cache.read("#{request_path}-#{request.ip}")[three_minutes_ago]).to be_nil
        expect(Rails.cache.read("#{request_path}-#{request.ip}")[two_minutes_ago]).to be_nil
        expect(Rails.cache.read("#{request_path}-#{request.ip}")[one_minute_ago]).to eq(3)

        # new entry for current request minute
        expect(Rails.cache.read("#{request_path}-#{request.ip}")[new_entry_timestamp]).to eq(1)

        # tipping point, ip gets BLOCKED for the next 1 minute
        get :random

        expect(response.status).to be(429)
        expect(response.body).to include("Rate limit exceeded. Try again in approximately #{expected_wait_seconds} seconds")
        expect(Rails.cache.read("#{request_path}-#{request.ip}")[0]).to eq(Tracker::IP_STATUS_BLOCKED)
        expect(Rails.cache.read("#{request_path}-#{request.ip}")[new_entry_timestamp]).to eq(2)
        expect(Rails.cache.read("#{request_path}-#{request.ip}")[blocked_till_begining_of_timestamp]).to eq(0)
      end
    end
  end

  describe 'When in Blocked state with future timestamp in cache' do
    let(:configuration) do
      JSON.parse({
        request_path: request_path,
        time_window: 2,
        max_allowed: 4,
        track_pathwise: true
      }.to_json, object_class: OpenStruct)
    end
    
    let(:existing_cache_data) do
      {
        0 => Tracker::IP_STATUS_BLOCKED,
        old_entry => 3,
        blocked_at => 2,
        blocked_till_begining_of_timestamp => 0
      }
    end

    context 'when invoked WITHIN the blocking period' do
      let(:assumed_time_now) { Time.zone.now + 1.minute }
      let(:old_entry) { (assumed_time_now - 2.minutes).change(:sec => 0).to_i }
      let(:blocked_at) { (assumed_time_now - 1.minute).change(:sec => 0).to_i }
      let(:blocked_till_begining_of_timestamp) { (assumed_time_now + 1.minute).change(:sec => 0).to_i }
      let(:expected_wait_seconds) { blocked_till_begining_of_timestamp - assumed_time_now.to_i }

      before do
        Timecop.freeze(assumed_time_now)
      end

      after do
        Timecop.return
      end
      
      it 'returns 429 - waiting for end of window' do
        allow(EndpointConfiguration).to receive(:find_by).and_return(configuration)
        allow(Rails.cache).to receive(:read).and_return(existing_cache_data)

        expect(Rails.cache.read("#{request_path}-#{request.ip}")[0]).to eq(Tracker::IP_STATUS_BLOCKED)
        expect(Rails.cache.read("#{request_path}-#{request.ip}")[blocked_till_begining_of_timestamp]).to eq(0)

        get :random

        expect(response.status).to be(429)
        expect(response.body).to include("Rate limit exceeded. Try again in approximately #{expected_wait_seconds} seconds")
      end
    end

    context 'when invoked AFTER the blocking period' do
      let(:timestamp_of_future_request) { (Time.zone.now + 3.minutes).change(:sec => 0).to_i }

      let(:assumed_time_now) { Time.zone.now + 2.minute }
      let(:old_entry) { (assumed_time_now - 3.minutes).change(:sec => 0).to_i }
      let(:blocked_at) { (assumed_time_now - 2.minute).change(:sec => 0).to_i }
      let(:blocked_till_begining_of_timestamp) { (assumed_time_now).change(:sec => 0).to_i }

      before do
        Timecop.freeze(assumed_time_now)
      end

      after do
        Timecop.return
      end
      
      it 'returns 200 - successfully waited for window to finish' do
        allow(EndpointConfiguration).to receive(:find_by).and_return(configuration)
        allow(Rails.cache).to receive(:read).and_return(existing_cache_data)

        expect(Rails.cache.read("#{request_path}-#{request.ip}")[0]).to eq(Tracker::IP_STATUS_BLOCKED)
        expect(Rails.cache.read("#{request_path}-#{request.ip}")[blocked_till_begining_of_timestamp]).to eq(0)

        get :random

        expect(response.status).to be(200)
        expect(Rails.cache.read("#{request_path}-#{request.ip}")[0]).to eq(Tracker::IP_STATUS_ACTIVE)
        expect(Rails.cache.read("#{request_path}-#{request.ip}")[blocked_till_begining_of_timestamp]).to eq(1)
      end
    end
  end
end