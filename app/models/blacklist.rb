class Blacklist
  include Mongoid::Document

  field :redis_key, type: String
  field :value, type: Hash
  field :status, type: String

  IP_STATUS_ACTIVE = 'ACTIVE'.freeze
  IP_STATUS_BLOCKED = 'BLOCKED'.freeze

  validates :redis_key, :uniqueness => true, :exclusion => { :in => %w(*) }

  def as_json(*a)
    to_hash.as_json(*a)
  end

  def to_hash
    {
      redis_key: redis_key,
      value: value_for_display,
      status: status,
    }
  end

  def value_for_display
    value.select{|k,v| k != "0"}
  end

  def self.newRecord(key, value)
    record = Blacklist.find_by(redis_key: key)
    if record.present?
      record.value = value
    else
      record = Blacklist.new({
        redis_key: key,
        value: value,
        status: value[0],
      })
    end
    record.save!
  end

  def self.update_from_cache
    Blacklist.all.each do |entry|
      value_in_cache = Rails.cache.read(entry.redis_key)
      if value_in_cache.nil? || value_in_cache[0] == IP_STATUS_ACTIVE
        entry.destroy!
      elsif value_in_cache[0] == IP_STATUS_BLOCKED
        entry.value = value_in_cache
        entry.save!
      end
    end 
  end
end