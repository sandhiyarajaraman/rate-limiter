class EndpointConfiguration
  include Mongoid::Document

  field :request_path, type: String
  field :time_window, type: Integer
  field :max_allowed, type: Integer
  field :track_pathwise, type: Boolean

  validates :request_path, :uniqueness => true
  validate :default_cant_be_pathwise

  def default_cant_be_pathwise
    errors.add(:track_pathwise, "cannot be true for a default request path (*)") if (request_path == "*") && (track_pathwise == true)
  end  

  def as_json(*a)
    to_hash.as_json(*a)
  end

  def to_hash
    {
      request_path: request_path,
      time_window: time_window,
      max_allowed: max_allowed,
    }
  end

  def pathwise_tracking_enabled
    track_pathwise ? "Yes" : "No"
  end

  def sample_key
    track_pathwise ? "#{request_path}-1.2.3.4" : "1.2.3.4"
  end

  def self.default_configuration
    default_conf = EndpointConfiguration.find_by(request_path: "*")
    if default_conf.nil?
      default_conf = EndpointConfiguration.new({
        request_path: "*",
        time_window: 60,
        max_allowed: 100,
        track_pathwise: false
      })
      default_conf.save!
    end
    default_conf
  end
end