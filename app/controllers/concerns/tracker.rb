module Tracker
  extend ActiveSupport::Concern

  included do
    before_action :track_request_ip!
  end

  IP_STATUS_ACTIVE = 'ACTIVE'.freeze
  IP_STATUS_BLOCKED = 'BLOCKED'.freeze

  private

  def track_request_ip!
    allowed, wait_seconds = request_within_limits?
    return if allowed
    render :json => {message: "Rate limit exceeded. Try again in approximately #{wait_seconds} seconds"}, status: :too_many_requests and return
  end

  def request_ip
    # params[:test_ip] || request.ip
    request.ip
  end

  def request_path
    "#{params[:controller]}/#{params[:action]}"
  end

  def get_configuration
    configuration = EndpointConfiguration.find_by(request_path: request_path)
    if configuration.present?
      configuration
    else
      EndpointConfiguration.default_configuration
    end
  end

  def request_within_limits?
    request_timestamp = Time.zone.now
    request_epoch = request_timestamp.change(:sec => 0).to_i
    ip = request_ip
    tracker_config = get_configuration
    key_in_cache = tracker_config.track_pathwise ? "#{request_path}-#{ip}" : ip

    request_data = Rails.cache.read(key_in_cache)
    
    if request_data.nil?
      # if no data exists, this is a fresh request -> serve without checking limits
      Rails.cache.write(key_in_cache, {0 => IP_STATUS_ACTIVE, request_epoch => 1})
      return true, 0
    elsif (request_data.keys.max > request_epoch) && request_data[0].eql?(IP_STATUS_BLOCKED)
      # if already blocked, keep blocking unless manually unblocked
      return false, (request_data.keys.max - request_timestamp.to_i)
    end

    # increment or initialize counter for that minute
    if request_data[request_epoch].present? 
      request_data[request_epoch] += 1 
    else
      request_data[request_epoch] = 1
    end

    # calculate last window average for ip
    window_start = request_epoch - tracker_config.time_window.minutes.to_i
    # purge old data - prevent cluttering of cache
    request_data.reject!{|k,v| k <= window_start}
    # sum of remaining entries is the number of requests made in the last tracking window
    last_window_avg = request_data.values.sum

    

    # decision to block or allow
    if last_window_avg > tracker_config.max_allowed
      # window size in minutes
      actual_window_size = (request_epoch - request_data.keys.min)/60
      dead_wait_period = tracker_config.time_window - actual_window_size
      next_possible_request_time = request_epoch + dead_wait_period.minutes.to_i

      request_data[next_possible_request_time] = 0
      request_data[0] = IP_STATUS_BLOCKED

      Rails.cache.write(key_in_cache, request_data)
      Blacklist.newRecord(key_in_cache, request_data)
      return false, (next_possible_request_time - request_timestamp.to_i)
    else
      request_data[0] = IP_STATUS_ACTIVE
      Rails.cache.write(key_in_cache, request_data)
      return true, 0
    end
  end
end