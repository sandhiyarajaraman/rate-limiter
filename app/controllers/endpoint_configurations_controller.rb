class EndpointConfigurationsController < ApplicationController
  before_action :set_configuration, only: [:show, :edit, :update, :destroy]

  def index
    @configurations = EndpointConfiguration.all
    respond_to do |format|
      format.html
    end
  end

  def new
    @configuration = EndpointConfiguration.new
  end

  def create
    @configuration = EndpointConfiguration.new(endpoint_configuration_params)
    if @configuration.save!
      respond_to do |format|
        format.html { redirect_to @configuration, notice: 'Endpoint Configuration was successfully created.' }
      end
    end
  rescue => ex
    error_message = @configuration ? @configuration.errors : ex.message
    respond_to do |format|
      format.html { render :new }
    end
  end

  def show
  end

  def edit
  end

  def update
    if @configuration.update!(endpoint_configuration_params)
      respond_to do |format|
        format.html { redirect_to @configuration, notice: 'Endpoint Configuration was successfully updated.' }
      end
    end
  rescue => ex
    error_message = @configuration.errors.present? ? @configuration.errors : ex.message
    respond_to do |format|
      format.html { render :edit }
    end
  end

  def destroy
    @configuration.destroy!
    respond_to do |format|
      format.html { redirect_to endpoint_configurations_path, notice: 'Endpoint Configuration was successfully destroyed.' }
    end
  rescue => ex
    error_message = @configuration.present? ? @configuration.errors : "Endpoint Configuration not found"
    respond_to do |format|
      format.html { render :edit }
    end
  end

  private

  def set_configuration
    @configuration = EndpointConfiguration.find(params[:id])
    rescue ActiveRecord::RecordNotFound => ex
      Rails.logger.warn "Unable to find Endpoint Configuration with request_path: #{params[:id]}"
      # raise ex
  end

  def endpoint_configuration_params
    if params[:endpoint_configuration].blank?
      raise StandardError, "Endpoint Configuration: request_path, time_window and max_allowed are not provided"
    else
      params.require(:endpoint_configuration).permit(:request_path, :time_window, :max_allowed, :track_pathwise)
    end
  end
end