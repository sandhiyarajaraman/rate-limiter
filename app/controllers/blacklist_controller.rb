class BlacklistController < ApplicationController

  # This controller is merely for convenience
  # This code is not optimised or perfected.
  def index
    Blacklist.update_from_cache
    @list = Blacklist.all
    respond_to do |format|
      format.html
    end
  end
end