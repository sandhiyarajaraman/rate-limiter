module Api
  class TargetsController < ApplicationController
    include Tracker

    def show
      render :json => {message: "GET Request registered for IP: #{request.ip}"}, status: :ok
    end

    def hit
      render :json => {message: "POST Request registered for IP: #{request.ip}"}, status: :ok
    end
  end
end