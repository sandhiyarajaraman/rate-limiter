Rails.application.routes.draw do
  root to: "endpoint_configurations#index"
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  namespace :api do
    resource :targets, only: [:show], :defaults => { :format => :json } do
      member do
        post :hit
      end
    end
  end

  resources :endpoint_configurations
  resources :blacklist, only: [:index]
end
