# README

This project attempts to implement a request **`Rate-Limiting`** module.

## Dependencies

Ruby version: 2.6.0

Rails version: 6.0.3.2

MongoDB: v4.4.0

Redis server: v 5.0.5 or above

## Overview

This application's key highlight is the module **`Tracker`**
inside **`app/controllers/concerns`**

This module can be included in any controller to act as a request rate-limiter, tracking requests by IP address.

This rate-limiting module implements the `Sliding Window Algorithm`.

### Using the Rate-Limiter module

As an example, please see code inside `app/controllers/api/targets_controller.rb`

We simply include the module like so:

```
include Tracker
```

When the `Tracker` module is included, it adds a `before_action` named **`track_request_ip!`** which implements the logic to decide whether or not a request from an IP should be entertained.

#### Storing Configurations

- Rate-Limit configurations are stored in **MongoDB**. The collection names in their corresponding environments would be

  - rate_limiter_development
  - rate_limiter_test

- The **collection** where the configurations are stored is called **`endpoint_configurations`**
- The collection where the blacklisted entries are stored is called **`blacklist`**

#### Storing Request counters

- We use Redis cache configured into our rails config to store/count/increment and decide if a request originating from an IP should be served/blocked.
- We use 2 strategies to store in redis:
  - [Using the **`ip-address`** alone as `redis-key`](#under-the-default-config)
  - [Using a combination of the **`request-path`** and **`ip-address`** as `redis-key`](#path-specific-configurations)

#### Assumptions

- We round the request timestamp to the **lower-end minute**. So a request at `05:12:32` is stored as a request made at `05:12:00`. This way requests within a minute are incremented. This make it easy to calculate sums and averages over longer windows like `hours` or `days`

- Limitations of this assumption: In case the IP is blocked at `05:12:59` and must only be unblocked after 3 minutes at `05:15:59`, _this assumption will cause the IP to be unblocked at `05:15:00` - 59 seconds earlier than it should have._

- I still feel it is better to round-off the seconds while storing to redis, because the performance benefit of iterating-over, adding, deleting and writing fewer records outweighs the precision of seconds in real-time high-availability systems.

### Running Tests:

To run tests for `Tracker` do:

```
bundle exec rspec
```

## UI Endpoints & Implementation Logic

### List endpoint-configurations

```
http://localhost:3000/endpoint_configurations
```

![alt text](./readme-doc/endpoint-config-list.png 'Default configuration')

The wildcard request-path `*` is a default configuration added by the application. If a controller's action (path) does not have an exclusive configuration, this default configuration is used.

#### Under the default config

- The request-ip is tracked with an incrementing counter, even if the IP hits different endpoints of the same system.

  - So if `IP: 1.2.3.4` first hits `api/actionA` and then `api/actionB`, the redis data will look like:

    ```
    1.2.3.4: {
      1597556940: 1
      1597557060: 1
    }
    ```

#### Path specific configurations

- We can create specific request tracking configurations for each endpoint in our controller when we want to separate the counting logic.

  For example:

  - We may be more lenient with a `GET` endpoint allowing 120 requests per minute.
  - We may want to safeguard a `POST` endpoint from brute-force attacks by allowing only 100 requests in 60 minutes.

  To help achieve this, we configure each request path with specific `time-window/max-allowed` values using this Endpoint `http://localhost:3000/endpoint_configurations/new`

  Here, for ease of demonstration, I am configuring the POST endpoint `/api/targets/hit` with these values -> note that `Track pathwise` is :white_check_mark:

  ![alt text](./readme-doc/new-configuration.png 'New configuration')
  ![alt text](./readme-doc/custom-endpoint.png 'New custom endpoint')

  Now, when the same IP hits this endpoint multiple times, the request is tracked in redis like so :point_down: Note the `redis-key` has the `request-path` as prefix to the IP address

  ```
  "api/targets/hit-1.2.3.4": {
    1597560300=>2,
    1597560360=>4,
    1597560420=>0,
    0=>"BLOCKED"
  }
  ```

### Show Blacklisted IPs (with their corresponding redis-key)

```
http://localhost:3000/blacklist
```

Once an IP (alone) or an IP hitting a specific path (with endpoint-configuration) have reached their maximum limit allowed, they get blocked for the remaining time within that window.

The list of IPs / Path+IPs in blocked status can be seen from this endpoint

![alt text](./readme-doc/blacklist.png 'Blacklist')

In the above image :point_up: the value can be interpreted as follows (Time rounded to the minute):

```
{
  "1597554300"=>3, -> requests at 2020-08-16 15:05:00
  "1597554360"=>8, -> requests at 2020-08-16 15:06:00
  "1597554480"=>0 -> 2020-08-16 15:08:00 is when the next request will be allowed. Blocked until then.
}
```

### Simple results of success/blocked on Postman

#### Successful

![alt text](./readme-doc/success.png 'Success')

#### Blocked after limit is reached

![alt text](./readme-doc/blocked.png 'Blocked')
